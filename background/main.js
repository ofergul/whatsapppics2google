//This script handles the creation of the start if the application via a context menu on the whatsapp web page.
//When the application is started, will also install the proxy content script to access the WhatsApp API to retrive the profiles

//Wrap the error in accessing the WhatsappAPI
function onWhatsAppAccessError(err){
  browser.notifications.create({
    "type": "basic",
    "title": "Error",
    "message": "You need to login to WhatsApp before using this function"
  });
  return;  
}

//Add the context menu to the whatsapp web page
var WapTab;
browser.menus.create({
  id: "WhatsAppPics2GoogleContextMenu",
  title: "Sync contacts pictures",
  icons: {
      "16": "/icons/Contacts-icon-16.png",
      "32": "/icons/Contacts-icon-32.png"
    },
  documentUrlPatterns: ["https://web.whatsapp.com/*"],
  onclick:(i,t)=>{
	//Store WhatsApp web tab for later use
	WapTab=t;
	
	//Inject the proxy script into the whatsapp tab
	browser.tabs.executeScript(WapTab.id,{
      file: "/js/whatsapp_comm.js",
      allFrames: true
    }).then((r)=>{
		if(!(r&&r[0]&&r[0]==true)){
			//User is not logged in, do nothing
			return onWhatsAppAccessError();
		}
		//Create the window panel
		let createData = {
		type: "detached_panel",
		  url: "window.html",
		};
		let creating = browser.windows.create(createData);
		creating.then((w) => {
		  console.log("The detached panel has been created");
	  });	
	},onWhatsAppAccessError);
  }
});

