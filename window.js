function logError(error) {
  browser.notifications.create({
    "type": "basic",
    "title": "Error",
    "message": "Error in accessing your Google contacts. Have you denied access to this app? ("+error+")"
  });
  return; 
}

function loadComplete() {
  document.getElementById("loaderwrap").style.display = "none";
  document.getElementById("page").style.display = "block";
  return;
}

function loadLog(message) {
  document.getElementById("loadertext").innerText = message;
  return;
}

function loadError(message) {
  var pe=document.getElementById("pageerror");
  pe.innerText = message;
  pe.style.display = "block";
  return;	
}

function getContactsList() {
  const requestURL = "https://content-people.googleapis.com/v1/people/me/connections?personFields=names,photos,phoneNumbers&pageSize=2000";
  const requestHeaders = new Headers();
  requestHeaders.append('Authorization', 'Bearer ' + accessToken);
  const driveRequest = new Request(requestURL, {
    method: "GET",
    headers: requestHeaders
  });

  return fetch(driveRequest).then((response) => {
    if (response.status === 200) {
      return response.json();
    } else {
      throw response.status;
    }
  });
}

var totalContacts=0;
var contactsLoaded=false;
function writepage(contactsList) {
	contactsList.connections.forEach(function(v,idx,arr){
		if(idx===arr.length-1) contactsLoaded=true;
		if(v.phoneNumbers && v.names){
			//Insert row into the table row
			var title=v.names[0].displayName;
			var contactid="";
			var contactpic="";
			for (k in v.photos) {
				if(v.photos[k].metadata.primary && v.photos[k].metadata.source && v.photos[k].metadata.source.type && v.photos[k].metadata.source.type=="CONTACT" && v.photos[k].metadata.source.id){
				  contactpic=v.photos[k].url;
				  contactid=v.photos[k].metadata.source.id;
				  break;
				}
			}
			if(contactpic==""){
				//this contact has no defined contact primary pic
				return
			}
			var utels=[];
			for (k in v.phoneNumbers) {
				if(v.phoneNumbers[k].canonicalForm) {
				  utels.push(v.phoneNumbers[k].canonicalForm.replace(/^\+/gm,''));
				}
			}
			if(utels.length==0) return;
			totalContacts=totalContacts+utels.length;
			insertinpage(title,contactid,contactpic,utels);
			for (k in utels){
			  browser.tabs.sendMessage(WapTab.id,{"type": "WhatsAppContactPicGet", "tel": utels[k]});
			}
		}
	});
	return;
}  

//Wrapper to write a contact into the contact list
function insertinpage(contactname,contactid,contactpic,tels) {
	var row=document.createElement("tr");
	row.id=contactname;
	var cell1 = document.createElement("td");
    var cell2 = document.createElement("td");
    var cell3 = document.createElement("td");
	var textnode1=document.createTextNode(contactname);
	var textnode2=document.createElement("img");
	textnode2.setAttribute("src",contactpic);
	textnode2.setAttribute("id","glcpic"+contactid);
	textnode2.setAttribute("class","panel-contacts-goppic");
	var textnode3=document.createElement("div");
	textnode3.setAttribute("class","panel-contacts-wappic-wrapper");
	for (t in tels){
	  var textnode3a=document.createElement("img");
	  textnode3a.setAttribute("id","wappic"+tels[t]);
	  textnode3a.setAttribute("class","panel-contacts-wappic-loading");
	  textnode3a.setAttribute("title","WhatsApp pic for +"+tels[t]);
	  textnode3a.setAttribute("data-contactid",contactid);
	  textnode3a.setAttribute("data-tel",tels[t]);;
	  textnode3.appendChild(textnode3a);
    }
	cell1.appendChild(textnode1);
	cell2.appendChild(textnode2);
	cell3.appendChild(textnode3);
	row.appendChild(cell1);
	row.appendChild(cell2);
	row.appendChild(cell3);
	document.getElementById("contacts-list").appendChild(row);
}

//Sorting of the contacts table
function sortTable() {
  var table, rows, switching, i, x, y, shouldSwitch;
  table = document.getElementById("panel-contacts");
  switching = true;
  /* Make a loop that will continue until
  no switching has been done: */
  while (switching) {
    // Start by saying: no switching is done:
    switching = false;
    rows = table.rows;
    /* Loop through all table rows (except the
    first, which contains table headers): */
    for (i = 1; i < (rows.length - 1); i++) {
      // Start by saying there should be no switching:
      shouldSwitch = false;
      /* Get the two elements you want to compare,
      one from current row and one from the next: */
      x = rows[i].getElementsByTagName("TD")[0];
      y = rows[i + 1].getElementsByTagName("TD")[0];
      // Check if the two rows should switch place:
      if (x.innerHTML.toLowerCase() > y.innerHTML.toLowerCase()) {
        // If so, mark as a switch and break the loop:
        shouldSwitch = true;
        break;
      }
    }
    if (shouldSwitch) {
      /* If a switch has been marked, make the switch
      and mark that a switch has been done: */
      rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);
      switching = true;
    }
  }
  //Loading is complete now, give it some time to reorder the drawing then show the page
  setTimeout(loadComplete,500);
}

//Update the google contact pic
function updateGoogleContactPic(event) {
  var phoneNumber=event.target.getAttribute("data-tel");
  var contactid=event.target.getAttribute("data-contactid");
  var img = document.getElementById("wappic"+phoneNumber).getAttribute('src');
  fetch(img).then(function(response) {
	if (response.status === 200) {
      return response.blob();
    } else {
      throw response.status;
    }
  }).then(function(blob) {
      //Put the image into the photo
	  const requestURL = "https://www.google.com/m8/feeds/photos/media/default/"+contactid+"?alt=json";
	  const requestHeaders = new Headers();
	  requestHeaders.append('Authorization', 'Bearer ' + accessToken);
	  requestHeaders.append('GData-Version', '3.0');
	  requestHeaders.append('If-Match','*');
	  requestHeaders.append('Content-Type','image/jpeg');
	  var driveRequest = new Request(requestURL, {
		method: "PUT",
		headers: requestHeaders,
		body: blob
	  });

	  return fetch(driveRequest).then((response) => {
		if (response.status === 200) {
		  return response.json();
		} else {
		  throw response.status;
		}
	  }).then(()=>{
	    //Update source image
		document.getElementById("glcpic"+contactid).setAttribute('src',img);
	  }).catch(logError);;
  })
}

//Message listener for WhatsApp pics
browser.runtime.onMessage.addListener(function (message,sender,sendResponse) {
  //console.log(message);
  if (message.type && sender.tab.id==WapTab.id) {
    if (message.type == "WhatsAppPicResolved" && message.data.tel) {
	  var picimg=document.getElementById("wappic"+message.data.tel);
	  if(message.data.pic){
	    picimg.setAttribute("src",message.data.pic);
	    picimg.setAttribute("class","panel-contacts-wappic");
		picimg.addEventListener('click', updateGoogleContactPic);
	  }else{
		picimg.setAttribute("class","panel-contacts-wappic-nopic");  
	  }
    };
  };
});

function checkIfLoaded() {
  //To check if the page loaded, get all contacts loaded and see if they have a WhatsApp pic assigned
  if((new Date().getTime() - loadStart.getTime()>60000)){
	loadError("Error: Timeout during load. Your contacts list may be incomplete");
	loadComplete();
	return;
  }
  if(!contactsLoaded){
	loadLog("Loading contacts ("+totalContacts+")...");
	setTimeout(checkIfLoaded,100);
	return;	  
  }
  var stillLoading=document.getElementsByClassName("panel-contacts-wappic-loading").length;
  if(stillLoading==0){
	loadLog("Sorting contacts...");
	setTimeout(sortTable,50);
    return  	  
  }
  loadLog("Loading contacts ("+(totalContacts-stillLoading)+"/"+totalContacts+")...");
  setTimeout(checkIfLoaded,100);
  return;
}

//Start page load
var accessToken;
var WapTab = browser.extension.getBackgroundPage().WapTab;
var loadStart = new Date();
function start() {
  //Load the list of contacts
  loadLog("Loading contacts...");
  getAccessToken().then((r)=>accessToken=r).then(getContactsList).then(writepage).catch(logError);
  
  //Start routine to check completed loading of whatsapp images...
  setTimeout(checkIfLoaded,100);
}
start();