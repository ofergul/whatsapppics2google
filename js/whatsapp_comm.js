//This functions inject the code of the whatsapp_getpic.js script, which is a wrapper of the WhatsApp API to get a profile picture, then acts as proxy between
//the injected whatsapp_getpic.js script and the extension popup page

// Load the GetPic script (this is obtained by obfuscating whatsapp_getpic.js code with https://www.danstools.com/javascript-obfuscate/index.php )
window.eval(function(p,a,c,k,e,d){e=function(c){return(c<a?'':e(parseInt(c/a)))+((c=c%a)>35?String.fromCharCode(c+29):c.toString(36))};if(!''.replace(/^/,String)){while(c--){d[e(c)]=k[c]||e(c)}k=[function(e){return d[e]}];e=function(){return'\\w+'};c=1};while(c--){if(k[c]){p=p.replace(new RegExp('\\b'+e(c)+'\\b','g'),k[c])}}return p}('5 d={};9 o(){5 j=0;5 f=0;5 a=0;9 m(){e Z X((l)=>{V 3=D.C("E");t["p"]([],{[3]:9(q,6,k){l(k.c)}},[3])})}5 1=m().B;I(5 2 v 1){4(1[2].6){4(1[2].6.w){j=1[2].3.h(/"/g,\'"\')}4(1[2].6.u){f=1[2].3.h(/"/g,\'"\')}4(1[2].6.b){4(1[2].6.b.s){a=1[2].3.h(/"/g,\'"\')}}}}8.7("10 = "+j);8.7("U = "+f);8.7("W = "+a);4(a==0){8.7(`Y:S R M q`);e}9 n(3){e p([],N,[3])}d=n(a).b;8.7("G P Q O.")}9 K(i){d.s.L(i+"@c.T").J(r=>t.y({z:"x",A:i,H:r.F},"*"))}o();',62,63,'|modules|key|id|if|var|exports|log|console|function|store_id|default||WhatsAppPageStore|return|prepareRawMedia_id||replace|phonenumber|createFromData_id|__webpack_require__|resolve|getAllModules|_requireById|WhatsAppPageStoreInit|webpackJsonp|module|res|Wap|window|prepRawMedia|in|createFromData|WhatsAppContactPic|postMessage|type|tel|_value|uniqueId|_|fakeModule_|eurl|WhatsApp|pic|for|then|GetWhatsAppContactPic|profilePicFind|storeID|null|initialized|Get|image|find|Cannot|us|PrepRawMediaID|const|StoreID|Promise|ERROR|new|CreateFromDataID'.split('|'),0,{}));

//Add proxy event listener, forwarding the pic to the popup page
window.addEventListener("message", function(event) {
  if (event.source == window &&
      event.data &&
      event.data.type == "WhatsAppContactPic") {
		  //forward message to content script
      event.data;
	  browser.runtime.sendMessage("WhatsAppPics2Google@noid.com",{"type": "WhatsAppPicResolved","data": event.data},{});
  }
  //console.log(event);
});

//Add proxy event listener, forwarding the popup page request for a pic to the whatsapp api
browser.runtime.onMessage.addListener(request => {
  if(request.type && request.type=="WhatsAppContactPicGet" && request.tel){
	//console.log("Running "+'GetWhatsAppContactPic("'+request.tel+'");');
	window.eval('GetWhatsAppContactPic("'+request.tel+'");');
  }
  return;
});

//Return the connection status
window.eval("WhatsAppPageStore.Conn.__x_connected");


